#!/usr/bin/python
# -*- coding: utf-8 -*-

from discord.ext import commands
import discord
import random
import aiohttp
import json
import asyncio
import requests

class Fun:

    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        name="sebisauce",
        aliases=("sebi",),
        brief="Get some random sebi sauceage into your life!")
    async def sebisauce_gen(self, ctx):
        """
        Get a image related to sebi.
        sebi is a random guy with perfect code related jokes.
        """
        await ctx.trigger_typing()
        url = "http://ikbengeslaagd.com/API/sebisauce.json"
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                source = await response.json(encoding="utf8")

        total_sebi = 0
        for key in dict.keys(source):
            total_sebi += 1

        im = random.randint(0, int(total_sebi) - 1)

        await ctx.send(
            embed=discord.Embed(
                title="\t", description="\t", color=self.bot.embed_color
            ).set_image(url=source[str(im)])
        )
    async def on_message(message):
        if not message.author.bot and (message.server == None or self.bot.user in message.mentions):
            await self.bot.send_typing(message.channel)
            txt = message.content.replace(message.guild.me.mention,'') if message.server else message.content
            r = json.loads(requests.post('https://cleverbot.io/1.0/ask', json={'user':user, 'key':key, 'nick':'frost', 'text':txt}).text)
            if r['status'] == 'success':
            await message.channel.send(r['response'])

print('Starting...')
requests.post('https://cleverbot.io/1.0/create', json={'user':user, 'key':key, 'nick':'frost'})



def setup(bot):
    bot.add_cog(Fun(bot))
