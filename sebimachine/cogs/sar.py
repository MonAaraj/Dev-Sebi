#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

import discord
from discord.ext import commands

from sebimachine.utils.utils import pretty_list

from libneko import embeds


class Sar:
    def __init__(self, bot):
        self.bot = bot
        self.roles = (
            429725872673783809, # Python Helper
            429720493076250626, # JS Helper
            429720630070345728, # Other Helper
            477228989149216771, # Heroku Helper
            477229175191896066, # JVM Helper
            477229224080703489  # .NET Helper
            )

    # @commands.command(aliases=['selfrole', 'selfroles'])
    # async def sar(self, ctx, _type=None, _choice=None):
    #     # Examples:
    #     #    - ds!sar get 1 (adds Python Helper role)
    #     #    - ds!sar remove 3 (removes Other Helper role)
    #     #    - ds!sar list (shows all roles)
    #     #    - ds!sar (shows all roles)
    #     #    - ds!sar tst (sends a error message)

    #     def role_finder(role: str):
    #         return discord.utils.get(ctx.guild.roles, name=role)

    #     if _type == "list" or _type is None:
    #         em = embeds.Embed(title='List of Self Assigned Roles',
    #                            description='Usage: `ds!sar [ get | remove | list ] [ number ]`', colour=0x00FFFF)
    #         em.add_field(name='1. Python Helper', value='ds!sar get 1', inline=True)
    #         em.add_field(name='2. JS Helper', value='ds!sar get 2', inline=True)
    #         em.add_field(name='3. Other Helper', value='ds!sar get 3', inline=True)
    #         return await ctx.send(embed=em)

    #     roles = [role_finder('Python Helper'), role_finder('JS Helper'), role_finder('Other Helper')]

    #     if _choice not in ['1', '2', '3']:
    #         return await ctx.send('Enter a valid role number!')

    #     choice = int(_choice) - 1

    #     if _type == "get":
    #         await ctx.author.add_roles(roles[choice])
    #         await ctx.send('Added the role you requested')
    #     elif _type == "remove":
    #         await ctx.author.remove_roles(roles[choice])
    #         await ctx.send('Removed the role you requested')
    #     else:
    #         await ctx.send('Please provide a valid argument')
    #         await ctx.send('Please provide a valid argument')

    @commands.group(
        name="selfroles",
        aliases=['sar', 'selfassignroles', 'selfobtainroles'],
        brief="Self assignable roles 101", 
        case_insensitive=True)
    async def self_roles_group(self, ctx):
        """
        Get, remove or list (or set or revoke if you are a contributor :) ) roles!
        This provides an easy and ituitive way of getting all the available roles that you desire!
        Check the subcommands help pages for more info.
        """
        if not ctx.invoked_subcommand:
            return await self.list_roles.callback(self, ctx)

    @self_roles_group.command(
        name="list",
        aliases=['l'],
        brief="Lists the available self-roles")
    async def list_roles(self, ctx):
        roles = {discord.utils.get(ctx.guild.roles, id=int(id)) for id in self.roles}
        pretty_roles = pretty_list({r.mention for r in roles}, sep='\n:small_blue_diamond:  ')
        if pretty_roles:
            pretty_roles = ":small_blue_diamond:  " + pretty_roles
        await ctx.send(embed=embeds.Embed(
            colour=ctx.bot.embed_color,
            title=":label: Self-obtainable roles for this guild",
            description=pretty_roles or '`There are no self-assignable roles for this guild yet`'
        ))

    @self_roles_group.command(
        name="get",
        aliases=['g'],
        brief="Assigns the given role, if there's one")
    async def get_role(self, ctx, *, role: discord.Role):
        """
        Gets you the role you specified if it exists and is available!
        You can check wich roles are available for self-assigning by doing `ds!sar list` or simply `ds!sar`
        """
        try:
            if role.id in self.roles and role not in ctx.author.roles:
                    await ctx.author.add_roles(role, reason="Self-assigned role")
                    await ctx.send(embed=embeds.Embed(
                        description=f"<:checkmark:463714900020166657> Successfuly assigned {role.mention}!",
                        timestamp=None))
            else:
                await ctx.send(embed=embeds.Embed(
                    description="<:crossmark:463714900020166657> That role is either non-existent, not self-obtainable or you already have it!",
                    timestamp=None))
        except discord.errors.Forbidden:
            await ctx.send(embed=embeds.Embed(
                description="<:crossmark:463714900020166657> Missing the `MANAGE_ROLES` permission! Ask the admins to give me it first, then try again!",
                timestamp=None))
        except:
            await ctx.send(embed=embeds.Embed(
                description="<:crossmark:463714900020166657> Can't perform this action right now due to a databse error!",
                timestamp=None))

    @self_roles_group.command(
        name="remove",
        aliases=['rm'],
        brief="Unassigns the given role, if there's one")
    async def remove_role(self, ctx, *, role: discord.Role):
        """Removes the specified role from you if you have if, of course."""
        try:
            if role.id in self.roles and role in ctx.author.roles:
                    await ctx.author.remove_roles(role, reason="Self-assigned role")
                    await ctx.send(embed=embeds.Embed(
                        description=f"<:checkmark:463714900020166657> Successfuly unassigned {role.mention}!",
                        timestamp=None))
            else:
                await ctx.send(embed=embeds.Embed(
                    description="<:crossmark:463714900020166657> You either don't have that role or it's not self-obtainable!",
                    timestamp=None))
        except discord.errors.Forbidden:
            await ctx.send(embed=embeds.Embed(
                description="<:crossmark:463714900020166657> Missing the `MANAGE_ROLES` permission! Ask the admins to give me it first, then try again!",
                timestamp=None))                
        except:
            await ctx.send(embed=embeds.Embed(
                description="<:crossmark:463714900020166657> Can't perform this action right now due to a databse error!",
                timestamp=None))    


# If you want to do a more complete sar command just replace the redis database stuff for whatever db stuff u want
# @commands.has_permissions(manage_roles=True)
# @self_roles_group.command(
#     name="set",
#     aliases=['s'],
#     brief="Sets an existing role as a self-obtainable role")
# async def set_self_role(self, ctx, *, role: discord.Role):
#     try:
#         await self.bot.db_pool.sadd(f"guilds:{ctx.guild.id}:self_roles", role.id)
#         await ctx.send(embed=embeds.Embed(
#             description=f"<:checkmark:463714900020166657> Successfuly set {role.mention} as a self-obtainable role!",
#             timestamp=None))
#     except:
#         await ctx.send(embed=embeds.Embed(
#             description="<:crossmark:463714900020166657> Can't perform this action right now due to a databse error!",
#             timestamp=None))


# @commands.has_permissions(manage_roles=True)
# @self_roles_group.command(
#     name="revoke",
#     aliases=['rv'],
#     brief="Revokes the self-role qualification from a self-role")
# async def revoke_self_role(self, ctx, *, role: discord.Role):
#     try:
#         await self.bot.db_pool.sadd(f"guilds:{ctx.guild.id}:self_roles", role.id)
#         await ctx.send(embed=embeds.Embed(
#             description=f"<:checkmark:463714900020166657> Successfuly set {role.mention} as a self-obtainable role!",
#             timestamp=None))
#     except:
#         await ctx.send(embed=embeds.Embed(
#             description="<:crossmark:463714900020166657> Can't perform this action right now due to a databse error!",
#             timestamp=None))   


def setup(bot):
    bot.add_cog(Sar(bot))
