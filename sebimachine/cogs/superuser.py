#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

from libneko.extras import superuser


class CommunitySuperuserCog(superuser.SuperuserCog):
    async def owner_check(self, ctx):
        return ctx.author.id in ctx.bot.ownerlist


def setup(bot):
    bot.add_cog(CommunitySuperuserCog())
