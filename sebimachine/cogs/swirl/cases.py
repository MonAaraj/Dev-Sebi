#!/usr/bin/python
# -*- coding: utf-8 -*-

from discord.ext import commands
import discord

# from libneko import asyncinit
from dataclasses import dataclass

from libneko import embeds

import datetime


@dataclass
class MainCase:
    id: int
    executor: discord.Member
    target: discord.Member
    created_at: datetime.datetime
    reason: str = None
    log_id: int = None


@dataclass
class KickCase(MainCase):
    booties: str = None

    async def log(self, ctx):
        boots = self.booties or "an old pair of boots"
        log = embeds.Embed(
            title=":boot: Kick",
        description=f"{self.executor.mention} kicked {self.target} (ID: {self.target.id}) with {boots}!")
        log.add_field(
            name="Reason",
            value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{id}")
        
        mod_log_ch = ctx.bot.get_channel(462903595893915649)
        await mod_log_ch.send(embed=log)


@dataclass
class BanCase(MainCase):
    duration: int = None

    async def log(self, ctx):
        duration = self.duration or "infinit"
        log = embeds.Embed(
            title="<:ban_hammer:480415759848439818> Ban",
        description=f"{self.executor.mention} banned {self.target} (ID: {self.target.id}) for {duration} hour(s)!") # This is debatable
        log.add_field(
            name="Reason",
            value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{id}")
        
        mod_log_ch = ctx.bot.get_channel(462903595893915649)
        await mod_log_ch.send(embed=log)


@dataclass
class MuteCase(MainCase):
    duration: int = None

    async def log(self, ctx):
        duration = self.duration or "infinit"
        log = embeds.Embed(
            title=":mute: Mute",
            description=f"{self.executor.mention} muted {self.target.mention} (ID: {self.target.id}) for {duration} hour(s)!") # This is debatable
        log.add_field(
            name="Reason",
            value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{id}")
        
        mod_log_ch = ctx.bot.get_channel(462903595893915649)
        await mod_log_ch.send(embed=log)


@dataclass
class UnmuteCase(MainCase):
    async def log(self, ctx):
        log = embeds.Embed(
            title=":unmute: Unmute",
            description=f"{self.executor.mention} unmuted {self.target.mention} (ID: {self.target.id})!")
        log.add_field(
            name="Reason",
            value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{id}")
        
        mod_log_ch = ctx.bot.get_channel(462903595893915649)
        await mod_log_ch.send(embed=log)


@dataclass
class Strike(MainCase):
    amount: int

    async def log(self, ctx):
        log = embeds.Embed(
            title=":triangular_flag_on_post: Strike",
            description=f"{self.executor.mention} striked {self.target.mention} (ID: {self.target.id}) **{self.amount}** time(s)!")
        log.add_field(
            name="Reason",
            value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{id}")
        
        mod_log_ch = ctx.bot.get_channel(462903595893915649)
        await mod_log_ch.send(embed=log)


@dataclass
class Pardon(MainCase):
    amount: int

    async def log(self, ctx):
        log = embeds.Embed(
            title=":flag_white: Strike",
            description=f"{self.executor.mention} pardoned {self.target.mention} (ID: {self.target.id}) **{self.amount}** strike(s)!")
        log.add_field(
            name="Reason",
            value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{id}")
        
        mod_log_ch = ctx.bot.get_channel(462903595893915649)
        await mod_log_ch.send(embed=log)


@dataclass
class MsgClear:
    id: int
    executor: discord.Member
    channel: discord.TextChannel
    created_at: datetime.datetime
    reason: str = None
    log_id: int = None
    amount: int
    criteria: str = None

    async def log(self, ctx):
        log = embeds.Embed(
            title=":wastebasket: Message Clear",
            description=f"{self.executor.mention} cleared **{self.amount}** messages in {self.channel.mention}!")
        log.add_field(
            name="Criteria",
            value=self.criteria or "`None, they all went to oblivion`")
        log.add_field(
            name="Reason",
            value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{id}")
        
        mod_log_ch = ctx.bot.get_channel(462903595893915649)
        await mod_log_ch.send(embed=log)
        